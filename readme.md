- Prerequisites: A GitLab account, a Linux machine (physical, VM or cloud), moderate git and command line experience.
- Time to complete: 1 hour.
- See an [example](https://twitter.com/asanabot) of a Twitter Bot created using this template.

# Twitter developer account

Apply for a [Twitter developer account](https://developer.twitter.com/en/apply-for-access.html). You'll need to justify your project but don't sweat the bio too much. Then create an app and make a note of the [two API keys](https://developer.twitter.com/en/apps/). Keep them to yourself.

# Installation

This procedure is orchestrated using a makefile and some bash scripts. So at a minimum you’ll need to have installed `make`.

```bash
sudo apt update && \
sudo apt install make git --yes
```

Fork this repo and make it private because we're about to store your Twitter API keys. You _could_ build it at this point but it will fail to authorize. So clone it on a Linux machine to carry out the authorization.

```bash
git clone https://gitlab.com/yourusername/gitlab-hosted-twurl-bot && \
cd gitlab-hosted-twurl-bot
```

# Authorization

Run `make` and if this is the first run it will install the required packages (ruby and twurl). Then paste your Twitter keys when prompted. When you're presented with a URL ctrl-click it to open it in a browser, then click through to the access PIN and paste it back into the terminal to complete the authorization. Upon completion your Twitter status will then be updated and you'll see a splurge of JSON in response.

```bash
$ make

Consumer API keys
Enter API key:
xxxxxxxx

Enter API secret key:
xxxxxxxx

Ctrl-click the URL below open it in a browser, then click through to the access PIN and paste it into the terminal to complete authorization:
Go to https://api.twitter.com/oauth/authorize?oauth_consumer_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&oauth_signature=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&oauth_signature_method=HMAC-SHA1&oauth_timestamp=xxxxxxxxxx&oauth_token=xxxxxxxxxxxxxxxxxxxxxxxxxxx&oauth_version=1.0 and paste in the supplied PIN

xxxxxxxx

Authorization successful

twurl -X POST -H api.twitter.com "/1.1/statuses/update.json?status=Wed 11 Sep 23:41:26 BST 2019"
```

**REMINDER TO CHECK YOUR FORK IS PRIVATE**

Now add your Twitter API keys: you'll need to use the force flag so it's clear what you're about to do. And this push will cause the pipeline to run and post another status update but from GitLab.

```bash
git add -f .twurlrc && \
git commit -m "Add my secret keys" && \
git push
```

# Schedules

You'll want your bot to run regularly so head back to GitLab and add a cron job via _GitLab > CI/CD > Schedules_. Note _all_ commits will trigger a status update, if you don't want this append "[skip ci]" to your commit message.

```bash
# E.g., four times a day
0 */4 * * *
```
