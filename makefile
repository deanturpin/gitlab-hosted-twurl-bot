# Normal install and run
all: .install .twurlrc tweet

# Instal but don't tweet
dry: .install .twurlrc

# Don't need to sudo when we're running in a GitLab instance
SUDO := sudo

.install:
	$(SUDO) apt update
	$(SUDO) apt install rubygems --yes
	$(SUDO) gem install twurl
	touch $@

# If there's no twurl config in this repo then we must authorize
# The resulting file will be copied into this repo
~/.twurlrc: .twurlrc
	@echo Copy local copy of twurl config to home dir
	cp $< $@

.twurlrc:
	@./authorize.sh
	cp ~/.twurlrc .

tweet: ~/.twurlrc
	twurl -X POST -H api.twitter.com "/1.1/statuses/update.json?status=$(shell date)"
