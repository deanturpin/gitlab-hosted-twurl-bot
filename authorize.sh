#!/bin/bash

# Prompt user for keys
echo
echo "######## Authorize ########"
echo
echo Consumer API keys
echo Enter API key:
read key
echo

echo Enter API secret key:
read secret
echo

echo "Ctrl-click the URL below open it in a browser, then click through to the access PIN and paste it into the terminal to complete authorization:"
twurl authorize --consumer-key $key --consumer-secret $secret
